import 'package:app/img_file_app.dart';
import 'package:flutter/material.dart';

class NamesListEmpty extends StatefulWidget {
  const NamesListEmpty({Key? key}) : super(key: key);
  static const String screenRout = 'names_empty_screen';

  @override
  _NamesListEmptyState createState() => _NamesListEmptyState();
}

class _NamesListEmptyState extends State<NamesListEmpty> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imagesNames['welcome']!,
              fit: BoxFit.cover,
              width: 200,
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'The list of names is empty',
              style: TextStyle(
                color: Colors.black,
                fontSize: 30,
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.amber.shade600,
        child: const Icon(
          Icons.add,
          size: 26,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
    );
  }
}
