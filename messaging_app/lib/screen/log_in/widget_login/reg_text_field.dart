import 'package:flutter/material.dart';

class RegTextField extends StatefulWidget {
  final TextEditingController textFieldController;
  final String hintTextField;
  final bool showPass;
  const RegTextField(
      {Key? key,
      required this.hintTextField,
      required this.textFieldController,
      required this.showPass})
      : super(key: key);

  @override
  _RegTextFieldState createState() => _RegTextFieldState();
}

class _RegTextFieldState extends State<RegTextField> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: widget.showPass,
      keyboardType: TextInputType.emailAddress,
      textAlign: TextAlign.center,
      onChanged: (value) {
        widget.textFieldController.text = value;
      },
      decoration: InputDecoration(
        hintText: widget.hintTextField,
        contentPadding:
            const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        border: const OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: Colors.orange),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(width: 2, color: Colors.blue),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(width: 2, color: Colors.orange),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
      ),
    );
  }
}
