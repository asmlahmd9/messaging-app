import 'package:app/screen/chat_screen.dart';
import 'package:app/screen/log_in/widget_login/reg_text_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:app/widget/my_button.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class SingInScreen extends StatefulWidget {
  const SingInScreen({Key? key}) : super(key: key);

  static const String screenRout = 'sigin_screen';

  @override
  _SingInScreenState createState() => _SingInScreenState();
}

class _SingInScreenState extends State<SingInScreen> {
  final _auth = FirebaseAuth.instance;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  bool showSpinner = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 180,
                child: Image.asset('assets/images/logo.png'),
              ),
              const SizedBox(
                height: 50,
              ),
              RegTextField(
                textFieldController: emailController,
                hintTextField: 'Enter Your Email',
                showPass: false,
              ),
              const SizedBox(
                height: 10,
              ),
              RegTextField(
                textFieldController: emailController,
                hintTextField: 'Enter Your Email',
                showPass: false,
              ),
              const SizedBox(
                height: 15,
              ),
              MyButton(
                  color: Colors.yellow[900]!,
                  title: 'Sing In ',
                  onPressed: () async {
                    setState(() {
                      showSpinner = true;
                    });

                    try {
                      final user = await _auth.signInWithEmailAndPassword(
                          email: emailController.text,
                          password: passwordController.text);

                      if (user != null) {
                        Navigator.pushNamed(context, ChatScreen.screenRout);
                        showSpinner = false;
                      }
                    } catch (e) {
                      print(e);
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
