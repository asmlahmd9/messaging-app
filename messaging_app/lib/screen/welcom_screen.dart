import 'package:app/screen/log_in/screen_login/register_screen.dart';
import 'package:app/screen/log_in/screen_login/singn_screen.dart';
import 'package:flutter/material.dart';

import 'package:app/widget/my_button.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  static const String screenRout = 'welcom_screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Column(
              children: [
                Container(
                  height: 180,
                  child: Image.asset('assets/images/logo.png'),
                ),
                const Text(
                  'MessageMe',
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w900,
                      color: Colors.indigo),
                )
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            MyButton(
              color: Colors.yellow.shade900,
              onPressed: () {
                Navigator.pushNamed(context, SingInScreen.screenRout);
              },
              title: 'Sing In',
            ),
            MyButton(
              color: Colors.blue.shade800,
              onPressed: () {
                Navigator.pushNamed(context, RegisterScreen.screenRout);
              },
              title: 'Register',
            ),
          ],
        ),
      ),
    );
  }
}
