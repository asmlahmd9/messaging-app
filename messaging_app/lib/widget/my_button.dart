import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  // required = توضع في حال نسيت ان تستخدمهم وفي وضع النل سيفتي
  final Color color;
  final String title;
  final VoidCallback onPressed;

  const MyButton(
      {required this.color, required this.title, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Material(
          borderRadius: BorderRadius.circular(7),
          color: color,
          elevation: 5,
          child: MaterialButton(
            onPressed: onPressed,
            minWidth: 200,
            height: 42,
            child: Text(
              title,
              style: const TextStyle(color: Colors.white),
            ),
          )),
    );
  }
}
